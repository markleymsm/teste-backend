const db = require('../db/models')
const Film = require("../db/models/index").Film;
const Vote = require("../db/models/index").Vote;
const sequelize = db.sequelize

exports.add = (req, res, next) => {
  if (req.user.tipoUsuario == 'admin') {
    Film.findOne({
      where: {
        nome: req.body.nome
      }
    }).then((user) => {
      if (user) {
        res.status(403).send("Filme já cadastrado!");
      } else {
        Film.create({
          nome: req.body.nome,
          diretor: req.body.diretor,
          genero: req.body.genero
        }).then((film) => {
          res.json(film);
        }).catch((err) => {
          res.status(404).send(`Erro ao salvar ${err.message}`)
        })
      }
    }).catch((err) => {
      res.status(404).send(err.message);
    })
  } else {
    res.status(401).send("Você não tem permissão para esta ação!");
  }
}

exports.edit = (req, res, next) => {
  if (req.user.tipoUsuario == 'admin') {
    Film
      .update(req.body, {
        where: {id: req.params.id}, returning: true, plain: true
      })
      .then(([, model]) => {
        res.json(model);
      })
  } else {
    res.status(401).send("Você não tem permissão para esta ação!");
  }
}

exports.delete = (req, res, next) => {
  if (req.user.tipoUsuario == 'admin') {
    Film
      .update({statusExclusao: 'deletado'}, {
        where: {id: req.params.id}, returning: true, plain: true
      })
      .then(([, model]) => {
        res.json(model);
      })
  } else {
    res.status(401).send("Você não tem permissão para esta ação!");
  }
}

exports.list = (req, res, next) => {
  let where = {
    statusExclusao: 'ativo',
  }
  if (req.query.diretor) where.diretor = req.query.diretor
  if (req.query.genero) where.genero = req.query.genero

  Film.findAll({
    where: where
  }).then(films => {
    if (films.length > 0)
      res.json(films)
    res.status(200).send("Não exite filmes cadastrados!");
  })
}

exports.detalhe = (req, res, next) => {
  Vote.findOne({
    where: {
      filmId: req.params.id
    },
    include: [ 'filme'],
    attributes: [
      [sequelize.fn('AVG', sequelize.col('voto')), 'mediaVotos'],
    ],
    raw: true,
    group: ['Vote.filmId'],
  }).then(film => {
    res.json(film)
  })
}