const Vote = require("../db/models/index").Vote

exports.add = (req, res, next) => {

  if (req.user.tipoUsuario == 'user') {
    Vote.create({
      voto: req.body.voto,
      filmId: req.body.filmId,
      userId: req.body.userId
    }).then((vote) => {
      res.json(vote)
    }).catch((err) => {
      res.status(404).send(`Erro ao salvar! ${err.message}`)
    })
  } else {
    res.status(401).send('Somente o usuário pode executar esta ação')
  }
}

exports.edit = (req, res, next) => {
  Vote
    .update(req.body, {
      where: {id: req.params.id}, returning: true, plain: true
    })
    .then(([, model]) => {
      res.json(model);
    })
}

exports.delete = (req, res, next) => {
  Vote.update({
      statusExclusao: 'deletado'
    },
    {
      where: {
        id: req.params.id
      },
      returning: true,
      plain: true
    }).then(([, model]) => {
    res.json(model);
  })
}

exports.list = (req, res, next) => {
  Vote.findAll({
    where: {
      statusExclusao: 'ativo'
    }
  }).then(votes => {
    res.json(votes)
  })
};

exports.getById = (req, res, next) => {
  Vote.findByPk(req.params.id)
    .then(vote => {
      res.json(vote)
    })

};