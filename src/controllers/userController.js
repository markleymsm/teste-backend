const bcrypt = require("bcrypt");
// import helpers from "../helpers/helpers";
const User = require("../db/models/index").User;
const cfg = require('../../config');
const jwt = require('jwt-simple');

exports.register = (req, res, next) => {

  User.findOne({
    where: {
      email: req.body.email
    }
  }).then((user) => {
    if (user) {
      res.status(400).send("Email existente, escolha outro!");
    } else {
      bcrypt.hash(req.body.senha, 10, (err, hash) => {
        User.create({
          email: req.body.email,
          nome: req.body.nome,
          sobreNome: req.body.sobreNome,
          tipoUsuario: req.body.tipoUsuario,
          senha: hash,
        }).then((user) => {
          res.json(user);
        });
      });
    }
  }).catch((err) => {
    res.status(404).send(err.message);
  })

}

exports.login = (req, res, next) => {
  let userReq = req.body;
  if (!userReq.email || !userReq.senha) {
    return res.status(401).send('Não Autorizado');
  }

  User.findOne({
    where: {email: userReq.email}
  }).then((user) => {
    if (!user) {
      res.status(401).send('Não Autorizado');
    }

    bcrypt.compare(userReq.senha, user.senha).then((equal) => {
      if (equal) {
        let payload = {id: user.id, tipoUsuario: user.tipoUsuario}
        let token = jwt.encode(payload, cfg.jwtSecret)
        res.json({user: userReq.email ,token: token})
      }else{
        res.status(401).send("Senha errada");
      }
    })

  })
}


exports.put = (req, res, next) => {
  User
    .update(req.body, {
      where: {id: req.params.id}, returning: true, plain: true
    })
    .then(([, model]) => {
      res.json(model)
    })
};

exports.delete = (req, res, next) => {
  User
    .update({statusExclusao: 'deletado'}, {
      where: {id: req.params.id}, returning: true, plain: true
    })
    .then(([, model]) => {
      res.json(model);
    })
};

exports.get = (req, res, next) => {
  User.findAll({where: {statusExclusao: 'ativo'}}).then(users => {
    res.json(users)
  })
};

exports.getById = (req, res, next) => {
  User.findByPk(req.params.id)
    .then(users => {
      res.json(users)
    })

};