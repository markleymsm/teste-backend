'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Vote.belongsTo(models.Film, {foreignKey: 'filmId', as: 'filme'})
    }
  };
  Vote.init({
    voto: DataTypes.INTEGER,
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    filmId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {model: 'Film', key: 'id'}
    },
    statusExclusao: {
      type: DataTypes.ENUM, values: ['ativo', 'deletado']
    }
  }, {
    sequelize,
    modelName: 'Vote',
  });
  return Vote;
};