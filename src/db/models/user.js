'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      senha: {type: DataTypes.STRING, allowNull: false},
      nome: {type: DataTypes.STRING},
      sobreNome: {type: DataTypes.STRING},
      email: {type: DataTypes.STRING, allowNull: false},
      tipoUsuario: {type: DataTypes.STRING},
      statusExclusao: {type: DataTypes.ENUM, values: ['ativo', 'deletado']}
    },
    {
      sequelize,
      modelName: 'User',
    });
  return User;
};