'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Film extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Film.hasMany(models.Vote, {as: 'votos'})
    }
  };
  Film.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nome: DataTypes.STRING,
    diretor: DataTypes.STRING,
    genero: {
      type: DataTypes.ENUM,
      values: ['terror', 'drama', 'comedia', 'ação']
    },
    statusExclusao: {
      type: DataTypes.ENUM,
      values: ['ativo', 'deletado'],
      defaultValue: 'ativo'
    }
  }, {
    sequelize,
    modelName: 'Film',
  });
  return Film;
};