'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      senha: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      nome: {
        type: Sequelize.STRING
      },
      sobreNome: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      tipoUsuario: {
        type: Sequelize.STRING
      },
      statusExclusao: {
        type: Sequelize.ENUM,
        values: ['ativo', 'deletado'],
        default: 'ativo'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};