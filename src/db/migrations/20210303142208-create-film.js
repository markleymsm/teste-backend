'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Films', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nome: {
        type: Sequelize.STRING
      },
      diretor: {
        type: Sequelize.STRING
      },
      genero: {
        type: Sequelize.ENUM,
        values: ['terror', 'drama', 'comedia', 'ação']
      },
      statusExclusao: {
        type: Sequelize.ENUM,
        values: ['ativo', 'deletado'],
        defaultValue: 'ativo'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Films');
  }
};