let passport = require('passport')
let passportJwtStrategy = require('passport-jwt').Strategy
let passportExtractJwt = require('passport-jwt').ExtractJwt

let User = require("../../db/models/index").User
let cfg = require('../../../config')

let params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: passportExtractJwt.fromAuthHeaderAsBearerToken()
}

let strategy = new passportJwtStrategy(params, function (jwt_payload, done) {
  let id = jwt_payload.id

  User
    .findByPk(id)
    .then(user => {
      if (!user) {
        return done(user)
      }
      return done(null, user)
    })

});

passport.use(strategy)

module.exports = passport