const voteController = require('../controllers/voteController')
const passport = require('../auth/auth')

module.exports = (app) => {
  app.post('/vote', passport.authenticate('jwt', {session: false}), voteController.add)
  app.put('/vote/:id', passport.authenticate('jwt', {session: false}), voteController.edit)
  app.delete('/vote/:id', passport.authenticate('jwt', {session: false}), voteController.delete)
  app.get('/vote', passport.authenticate('jwt', {session: false}), voteController.list)
  app.get('/vote/:id', passport.authenticate('jwt', {session: false}), voteController.getById)
}