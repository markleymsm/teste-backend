const UsuarioRoute = require('./userRoute')
const FilmRoute = require('./filmRoute')
const VoteRoute = require('./voteRoute')
const passport = require('../auth/auth')

module.exports = (app) => {
  app.use('/api', passport.authenticate('jwt', { session: false }))

  UsuarioRoute(app)
  FilmRoute(app)
  VoteRoute(app)
}