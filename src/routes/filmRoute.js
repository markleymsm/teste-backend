const filmController = require('../controllers/filmController')
const passport = require('../auth/auth')

module.exports = (app) => {
  app.post('/filme', passport.authenticate('jwt', {session: false}), filmController.add)
  app.put('/filme/:id', passport.authenticate('jwt', {session: false}), filmController.edit)
  app.delete('/filme/:id', passport.authenticate('jwt', {session: false}), filmController.delete)
  app.get('/filme/detalhe/:id', passport.authenticate('jwt', {session: false}), filmController.detalhe)
  app.get('/filmes', passport.authenticate('jwt', {session: false}), filmController.list)
}