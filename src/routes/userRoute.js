const userController = require('../controllers/userController')

module.exports = (app) => {
  app.post('/login', userController.login)
  app.post('/register', userController.register)
  // app.put('/usuario/:id', userController.put);
  // app.delete('/usuario/:id', userController.delete);
  // app.get('/usuarios', userController.get);
  // app.get('/usuario/:id', userController.getById);
}